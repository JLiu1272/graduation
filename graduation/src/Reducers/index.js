import { combineReducers } from 'redux'
import onlineMembers from './onlineMembers'

export default combineReducers({
    onlineMembers,
})