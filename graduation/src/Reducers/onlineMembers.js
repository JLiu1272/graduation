const onlineMembers = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_MEMBER':
            let stateCpy = {};
            stateCpy[action.name] = action.thumbnail;
            return Object.assign({}, state, stateCpy);
        case 'CLEAR_MEMBERS':
            return {};
        default:
            return state
    }
}

export default onlineMembers;