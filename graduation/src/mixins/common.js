var common = {
  convertName: function(name) {
    switch (name) {
      case "garyPriscilla":
        return "Gary and Priscilla";
      case "momDad":
        return "Mom and Dad";
      case "threeUncle":
        return "399 and Shelly Momo";
      default:
        return name;
    }
  }
};

export default common;
