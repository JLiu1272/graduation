import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from './Reducers';
import "./index.css";
import App from "./App";
import Room from "./Components/Room";
import * as serviceWorker from "./serviceWorker";
import { HashRouter, Route } from "react-router-dom";

const store = createStore(
  rootReducer,
);


ReactDOM.render(
  <Provider store={store}>
    <HashRouter>
      <Route exact path="/" component={App} />
      <Route
        path="/garyPriscilla"
        render={props => <App name="garyPriscilla" />}
      />
      <Route path="/kelly" render={props => <App name="Kelly" />} />
      <Route path="/momDad" render={props => <App name="momDad" />} />
      <Route path="/jefferson" render={props => <App name="Jefferson" />} />
      <Route path="/threeUncle" render={props => <App name="threeUncle" />} />
      <Route path="/me" render={props => <App name="me" />} />
      <Route path="/room" component={Room} />
    </HashRouter>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
