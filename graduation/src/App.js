import React from "react";
import "./CSS/App.css";
import Message from "./Components/Message";

function App(props) {
  return (
    <header className="App-header">
      <Message name={props.name} content="When you have completed reading this message. Please click Surprise Me! 
      This will ask you to take a photo of yourself, and will then bring you to a queing room. 
      There are other participants in this test. As participants enter into the room, 
      you will see their faces enter into this room in real time. Enjoy! Have a good time looking at their [Best photo moments]. 
      When all participants have entered the room, a Zoom session will start." />
    </header>
  );
}

export default App;
