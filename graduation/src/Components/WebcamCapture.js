import React, { useState, useEffect } from "react";
import Webcam from "react-webcam";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";

const videoConstraints = {
  width: "10%",
  height: "10%",
  facingMode: "user"
};

const WebcamCapture = props => {
  const [imgSrc, setImgSrc] = useState("");
  const { start, name } = props;
  let history = useHistory();

  const webcamRef = React.useRef(null);

  const capture = React.useCallback(() => {
    setImgSrc(webcamRef.current.getScreenshot());
  }, [webcamRef]);

  useEffect(() => {
    if (imgSrc !== "") {
      history.push({
        pathname: "/room",
        state: {
          members: [],
          name: name,
          thumbnail: imgSrc,
        }
      });
    }
  });

  if (start === "true") {
    return (
      <div>
        <div>
          <Webcam
            audio={false}
            ref={webcamRef}
            screenshotFormat="image/jpeg"
            videoConstraints={videoConstraints}
          />
        </div>
        <Button
          href=""
          variant="contained"
          color="primary"
          onClick={capture}
        >
          Capture Photo
        </Button>
        <img src={imgSrc} alt="" />
      </div>
    );
  } else {
    return <></>;
  }
};

export default WebcamCapture;
