import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import WebCapture from "./WebcamCapture";
import common from "../mixins/common";

const Message = props => {
  const [showCam, setShowCam] = useState("false");
  const name = common.convertName(props.name);

  const CardMsg = () => {
    return (
      <Card>
        <CardContent>
          <div className="shopping-list">
            <h1>Message for {name}</h1>
            <p>{props.content}</p>
            <p>Welcome {name}! This is a test. Thank you for participating. </p>
          </div>
          <WebCapture start={showCam} name={name} />
          <CaptureButton />
        </CardContent>
      </Card>
    );
  };

  const CaptureButton = () => {
    if (showCam === "true") {
      return <></>;
    } else {
      return (
        <Button
          variant="contained"
          color="primary"
          onClick={() => setShowCam("true")}
        >
          Surprise Me!
        </Button>
      );
    }
  };

  return (
    <div>
      <CardMsg name={name} showCam={showCam} />
    </div>
  );
};

export default Message;
