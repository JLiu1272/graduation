import React, { useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { useLocation } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';

import { addMember, clearMembers } from '../Actions';
import Links from "../Constants/Links.json";
import $ from "jquery";

import "../CSS/App.css";
import "../CSS/Room.css";

const MembersGrid = props => {
  const location = useLocation();
  let prevState = location.state;
  const [spacing] = React.useState(2);
  let onlineMembers = useSelector(state => state.onlineMembers);
  const dispatch = useDispatch();

  let ws = new WebSocket(Links.websocket);

  const restartSession = () => {
    ws.send("Clear::::");
    dispatch(clearMembers());
  }

  useEffect(() => {
    // When the React component first initialises
    if ($.isEmptyObject(onlineMembers)) {
      ws.onopen = () => {
        // If a person refreshes the page, 
        // don't read this
        if (prevState) {
          let data = "Members::::" + JSON.stringify({
            name: prevState.name,
            thumbnail: prevState.thumbnail,
          });
          // console.log(`Connected: Sending Data, ${data}`);
          ws.send(data);
        }
      };
      ws.onmessage = evt => {
        // on receiving a new member, add it to the list of members
        const { isFull, thumbnails, message, members } = JSON.parse(evt.data);
        if (isFull) {
          console.log("Start Zoom Session");
          window.location.href = Links.zoomLink;
        } else {
          console.log("Received Response: [" + members + "]");
          Object.entries(thumbnails).forEach(([name, thumbnail]) => {
            dispatch(addMember(name, thumbnail));
          })
        }
      };
      ws.onclose = () => {
        ws = new WebSocket(Links.websocket);
        console.log("disconnected");
      };
    }
  });

  return (
    <div className="App-header">
      <center className="restart_button">
        <Button
          onClick={restartSession}
          color="primary"
          variant="contained">
          Restart Session
        </Button>
      </center>
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Grid container justify="center" spacing={spacing}>
            {Object.entries(onlineMembers).map(([name, thumbnail]) => {
              let defaultImg = thumbnail === null ? "https://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" : thumbnail;
              return (
                <Grid key={name} item>
                  <Card className="root">
                    <CardMedia
                      className="media"
                      image={defaultImg}
                      title={name}
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                        {name}
                      </Typography>
                    </CardContent>
                  </Card>
                </Grid>
              )
            })}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

const Room = props => {
  return <MembersGrid />;
};

export default Room;
