export const addMember = (name, thumbnail) => {
    return {
        type: 'ADD_MEMBER',
        name,
        thumbnail
    }
};

export const clearMembers = () => {
    return {
        type: 'CLEAR_MEMBERS'
    }
}